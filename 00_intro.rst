============
Introducción
============

Existe un amplio abanico de gestores de bases de datos relacionales, desde la ligera SQLite hasta el potentísimo PostgreSQL. Naturalmente, muchas aplicaciones desarrolladas con PHP necesitan conectarse a estos gestores de bases de datos. El motor de PHP provee una interfaz ligera para acceder a bases de datos, la **PDO** (*PHP Data Objects* [Objetos de datos de PHP]). El soporte para PDO se provee mediante unos controladores específicos (*PDO Drivers*), que permiten a PHP interactuar con un gestor determinado. Dado que cada gestor tiene una características propias y específicas, PDO provee una capa de abstracción que expone una interfaz unificada independiente del gestor específico de conexión. Luego, es posible ejecutar sentencias SQL *relativamente* independiente del gestor que se utilice [#f1]_.

Si bien es cierto que gracias a PDO es posible preparar sentencias, enlazar datos con columnas y prevenir inyecciones de SQL, también es verdad que PDO es bastante genérica y las sentencias se ejecutan llamando al método ``execute()``, no existiendo una abstracción nativa sobre las sentencias SQL más comunes (``INSERT``, ``SELECT``, ``UPDATE``, ``DELETE``).

Cabe mencionar también que, buscando métodos para cumplir con patrones como el MVC, se han desarrollado potentes herramientas que proveen toda una capa orientada a objetos sobre las bases de datos: **los ORM**. Existen proyectos que indudablemente se benefician de la potencia de los ORM, pero otros no son tan grandes y utilizar un ORM resulta un tanto como matar moscas a cañonazos, o incluso, puede que se quiera desarrollar un ORM propio y no comenzar desde cero.

Tuza es precisamente esa capa sobre PDO que abstrae las operaciones ``CRUD`` exponiendo una interfaz orientada a objetos y una arquitectura ligera y extensible.

Arquitectura
============

Tuza se compone de cinco archivos que proveen una funcionalidad y esquema general para su utilización en cualquier proyecto.
Tuza se provee listo para su uso, basta con crear una instancia de la clase ``DbConnection`` (con una configuración mínima) para realizar una conexión a un gestor soportado y comenzar a realizar operaciones. No obstante Tuza es más que una clase de conexión con una interfaz para las operaciones ``CRUD``, es una biblioteca extensible con un esquema intuitivo y coherente.

.. image:: /_static/class_diagram.svg

La interfaz ``DbConnectionInterface``
-------------------------------------

La abstracción mínima que debe tener una clase de conexión se establece en ``DbConnectionInterface``. Dicha abstracción mínima no es más que la declaración de los métodos que abstraen las operaciones ``CRUD`` (``insert()``, ``select()``, ``update()``, ``delete()``), un método de execución de sentencias (``exec()``), un método que devuelva la última consulra realizada (``getLastQuery()``) y la abstracción de algunas operaciones típicas sobre bases de datos (``avg()``, ``count()``, ``max()``, ``min()``, ``sum()``).

Tuza implementa dicha interfaz en la clase ``DbConnection``, pero la idea de la interfaz es proveer al usuario de una base agnóstica para que, si por algún motivo prefiere realizar una implementación propia, no sólo parta de cero, sino que disponga de una base compatible con Tuza.

La interfaz ``DbConnctable``
----------------------------

Esta interfaz define el comportamiento mínimo que debería tener cualquier clase que se pretenda que soporte conexiones a bases de datos. Provee la declaración de un sólo método: ``getDbConnection()``. Y su definición debe devolver un objeto del tipo ``DbConnectionInterface``.

El trait ``DbAware``
--------------------

Para la interfaz ``DbConnctable``, se provee el trait ``DbAware`` que define una funcionalidad básica que cumple con dicha interfaz. El usuario bien puede optar por utilizar dicho trait, definir su propia implementación o una combinación de ambas.

La clase ``DbConnection``
-------------------------

Esta es la clase que contiene la *magia*. Es precisamente una implementación de la interfaz ``DbConnectionInterface``, con varias cosas más. El grueso de esta documentación es sobre la funcionalidad que provee esta clase.

La clase ``DbException``
------------------------

Finalmente, es inevitable que se desencadenen errores cuando se ejecutan sentencias. Por defecto, Tuza establece que el manejo de errores de PDO sea mediante Excepciones. ``DbConnection`` captura varias de las excepciones lanzadas por PDO y lanza excepciones ``DbException`` con información extra sobre la sentencia que falló.

.. rubric:: Footnotes

.. [#f1] Como todo, tiene sus salvedades.
