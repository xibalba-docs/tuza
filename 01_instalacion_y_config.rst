===========================
Instalación y configuración
===========================

Instalación
===========

-- Por Hacer --

Configuración
=============

Configurar Tuza es tremendamente sencillo, sólo se requiere proveer al constructor de ``DbConnection`` con un ``array`` de opciones. La configuración se define igual para todas las bases de datos soportadas, excepto para SQLite, que únicamente requiere definir el controlador y la ruta del archivo de la base de datos para funcionar.

Configurar SQLite
-----------------

.. code-block:: php

  <?php
  use xibalba\tuza\DbConnection;

  // Crear una instancia de conexión
  $dbConnection = new DbConnection([
    'database_type' => 'sqlite',
    'database_file' => 'sqlitedb.db'
  ]);

Configurar Tuza
---------------

La configuración mínima requiere que se defina:

* Controlador
* Servidor
* Nombre de la base de datos
* Credenciales de acceso

Controlador
^^^^^^^^^^^

Define uno de los controladores soportados por tuza, a continuación se muestra un listado con el nombre de cada base de datos soportada y la cadena de configuración correspondiente:

* SQLite: ``sqlite``
* MariaDB: ``mariadb``
* MySQL: ``mysql``
* PostreSQL: ``pgsql``
* Oracle: ``oracle``
* Microsoft SQL Server: ``sybase``, ``dblib``, ``sqlsrv``

Como puede verse, todas las cadenas son en minúsculas. La cadena debe definirse como se indica o de lo contrario Tuza no funcionará.

Las cadenas para MariaDB y MySQL pueden utilizarse indistintamente, es decir, se puede configurar ``mysql`` y uliziar MariDB.

Para SQL Server puede verse que se soportan tres cadenas distintas, esto es porque existen diversas formas de conectarse a SQL Server con PHP. El motor de PHP para Linux soporta ``sybase`` y ``dblib``, no obstante Microsoft ha publicado su propio controlador tanto para Windows como la Linux, y en tal caso debe utilizarse ``sqlsrv``.
