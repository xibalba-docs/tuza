======================
Consultas y sentencias
======================

Como se mencionó en la introducción, Tuza abstrae en la clase ``DbConnection`` las operaciones ``CRUD`` exponiendo una interfaz orientada a objetos de la misma mediante los siguientes métodos:

* ``insert()``
* ``update()``
* ``delete()``
* ``select()``

Insertar datos
==============

.. php:method:: insert(string $table, array $datas)

    Abstrae la sentencia SQL del mismo nombre, y consecuentemente, el método ``insert()`` permite la inserción de datos en tablas.

    :param string $table: Cadena que especifíca en que tabla se insertarán los datos.
    :param array $datas: Arreglo de datos a insertar.

El Arreglo de datos es un conjunto «clave-valor» donde cada «clave» corresponde con el nombre de una columna de la tabla y cada «valor» con un dato a insertar.

.. code-block:: php

  <?php

  $data = [
      'id' => uniqid(),
      'titulo' => 'Algún título de ejemplo',
      'autor' => 'Nombre del Autor',
      'formato' => 'pdf',
      'paginas' => 100,
      'coste' => 200.5
  ];

  $db->insert('tbl_tesis_ejemplo', $data);

También es posible insertar varios registros en una sola llamada al método ``insert()``

.. code-block:: php

  <?php

  $datas = [
    [
      'id' => uniqid(),
      'titulo' => 'Algún título de ejemplo 1',
      'autor' => 'Nombre del Autor',
      'formato' => 'pdf',
      'paginas' => 100,
      'coste' => 200.5
    ],
    [
      'id' => uniqid(),
      'titulo' => 'Algún título de ejemplo 2',
      'autor' => 'Nombre del Autor',
      'formato' => 'epub',
      'paginas' => 100,
      'coste' => 200.5
    ]
  ];

  $db->insert('tbl_tesis_ejemplo', $datas);

Actualizar datos
================

.. php:method:: update(string $table, array $datas, array $where)

    Abstrae la sentencia SQL del mismo nombre, y consecuentemente, el método ``update()`` permite la actualización de datos en tablas.

    :param string $table: Cadena que especifíca en que tabla se actualizarán los datos.
    :param array $data: Arreglo de datos a insertar.
    :param array $where: Arreglo de condiciones sobre la sentencia.

Al igual que con ``insert()``, el Arreglo de datos es un conjunto «clave-valor» donde cada «clave» corresponde con el nombre de una columna de la tabla y cada «valor» con un dato a actualizar.

El arreglo de condiciones (``$where``) corresponde a la cláusa ``WHERE``, y se estructura de manera similar al arreglo de datos, no obsante el arreglo de condiciones puedeser mucho más complejo. Para más detalles sobre como se estructura el arreglo de condiciones consulte la subsección de Condiciones correspondiente a ``select()``.

.. code-block:: php

  <?php

  $data = [
      'autor' => 'Nombre corregido del Autor',
      'formato' => 'pdf',
      'paginas' => 120,
      'coste' => 240
  ];

  $condiciones = ['id' => 'xyz'];

  $db->update('tbl_tesis_ejemplo', $data, $condiciones);

Eliminar datos
==============

.. php:method:: delete(string $table, array $where)

    Abstrae la sentencia SQL del mismo nombre, y consecuentemente, el método ``delete()`` permite la eliminación de datos en tablas.

    :param string $table: Cadena que especifíca de que tabla se eliminarán datos.
    :param array $where: Arreglo de condiciones sobre la sentencia.

.. code-block:: php

  <?php

  $condiciones = ['id' => 'xyz'];
  $db->delete('tbl_tesis_ejemplo', $condiciones);

Consultar datos
===============

.. php:method:: select(string $table, $columns, $where, $join)

    Abstrae la sentencia SQL del mismo nombre y, consecuentemente, el método ``select()`` permite la consulta de datos en tablas.

    :param string $table: Cadena que especifíca de que tabla se consultarán datos.
    :param string|array $columns: Cadena o arreglo que especifíca las columnas a consultar.
    :param string|array $where: Cadena o arreglo de condiciones sobre la sentencia.
    :param array $join: Arreglo de uniones sobre la sentencia.

.. code-block:: php

  <?php

  $allData = $db->select('tbl_tesis_ejemplo', '*');
  var_dump($allData);

El método ``select()`` es estructuralmente sencillo, pero muchas consultas requieren de condiciones *complejas*, no obstante gracias a que los parámetros de condiciones (``$where``) y unión (``$join``) son arreglos, permiten una gran potencia y flexibilidad en su estructuración.


Especificar columnas de consulta
--------------------------------

El parámetro ``$columns`` soporta el asterisco (``'*'``) o ``null`` para indicar que se desea obtener **todas** las columnas de la tabla.
Para especificar un conjunto de columnas a obtener se pasa un arreglo de cadenas con los nombres de las tablas:

.. code-block:: php

  <?php

  $allData = $db->select('tbl_tesis_ejemplo', ['id', 'titulo', 'autor']);
  var_dump($allData);

Es posible también especificar un **alias** para cada columna adjuntando a la cadena, entre paréntesis, dicho *alias*. En el arreglo de retorno se utilizará como clave el *alias* y no el nombre de la columna:

.. code-block:: php

  <?php

  $allData = $db->select('tbl_tesis_ejemplo', ['id(código)', 'titulo(etiqueta)', 'autor']);
  var_dump($allData[0]['etiqueta']);

Especificar condiciones de consulta
-----------------------------------

Las condiciones se especifican mediante un arreglo. Tuza soporta la mayoría de condiciones de SQL.

.. code-block:: php

  <?php

  $columns = ['id', 'titulo', 'autor'];

  $conditions = ['id' => 'xyz'];
  // Equivalente a:
  // WHERE id = 'xyz'

  $allData = $db->select('tbl_tesis_ejemplo', $columns, $conditions);
  var_dump($allData[0]['etiqueta']);

Semejanzas (``LIKE``)
^^^^^^^^^^^^^^^^^^^^^^

En SQL cuando se quiere especificar de una condición **no estricta**, sino más bien *semejante* se utiliza el operador ``LIKE``. Este operador es abstraido en Tuza por el caracter de la virgulilla encerrado entre corchetes (``[~]``) y concatenado a la columna de comparación.

.. code-block:: php

  <?php

  $columns = ['id', 'titulo', 'autor'];

  $conditions = ['autor[~]' => 'Ed'];
  // Equivalente a:
  // WHERE autor LIKE '%Ed%'

  $byAuthoresLike = $db->select('tbl_tesis_ejemplo', $columns, $conditions);

Para establecer la negación (``NOT LIKE``) se debe adjuntar el signo de admiración a la virgelilla.

.. code-block:: php

  <?php

  $columns = ['id', 'titulo', 'autor'];

  $conditions = ['autor[!~]' => 'Ed'];
  // Equivalente a:
  // WHERE autor NOT LIKE '%Ed%'

Conjunciones (``AND``)
^^^^^^^^^^^^^^^^^^^^^^

Las conjunciones se establecen con el operador ``AND``, para establecer conjunciones en Tuza se declara un arreglo de elementos a conjuntar y se asignan al arreglo de condiciones en la clave ``AND``.

.. code-block:: php

  <?php

  $columns = ['id', 'titulo', 'autor'];

  $conjunctions = ['id' => 'xyz', 'coste[>=]' => 150, 'autor[~]' => 'abc'];
  $conditions = ['AND' => $conjunctions];
  // Equivalente a:
  // WHERE id = 'xyz' AND coste >= 150 AND author LIKE '%abc%'

  $fetchedData = $db->select('tbl_tesis_ejemplo', $columns, $conditions);
  var_dump($fetchedData);

Disyunciones (``OR``)
^^^^^^^^^^^^^^^^^^^^^

Las disyunciones se establecen con el operador ``OR``, para establecer disyunciones en Tuza se declara un arreglo de elementos y se asignan al arreglo de condiciones en la clave ``OR``.

.. code-block:: php

  <?php

  $columns = ['id', 'titulo', 'autor'];

  $conjunctions = ['id' => 'xyz', 'coste[>=]' => 150, 'autor[~]' => 'abc'];
  $conditions = ['OR' => $conjunctions];
  // Equivalente a:
  // WHERE id = 'xyz' OR coste >= 150 OR author LIKE '%abc%'

  $fetchedData = $db->select('tbl_tesis_ejemplo', $columns, $conditions);
  var_dump($fetchedData);

Disyunciones (``IN``)
^^^^^^^^^^^^^^^^^^^^^

Dado que también es posible establecer un conjunto de disyunciones mediante el operador ``IN``, tuza soporta establecer el conjunto de elementos para dicho operador mediante un arreglo.

.. code-block:: php

  <?php

  $columns = ['id', 'titulo', 'autor'];

  $conjunctions = ['abc', 'xyz', 'vwm'];
  $conditions = ['autor' => $conjunctions];
  // Equivalente a:
  // WHERE id IN('abc', 'xyz', 'vwm')

  $fetchedData = $db->select('tbl_tesis_ejemplo', $columns, $conditions);
  var_dump($fetchedData);

Especificar uniones
-------------------

Tuza soporta las cuantro uniones de SQL, para establecer una (o más) uniones se provee como cuarto argumento en el método ``select()`` un arreglo donde la clave especifica el tipo de unión y tabla, y el valor es un arreglo que relaciona los campos (columnas) de la tabla principal con la table de unión.

El tipo de unión se establece mediante la combinación de corchetes y signos angulares (mayor y menor que):

* ``INNER JOIN``: ``[><]``
* ``FULL JOIN``: ``[<>]``
* ``LEFT JOIN``: ``[>]``
* ``RIGHT JOIN``: ``[<]``

En el siguiente ejemplo supóngase que el campo ``autor`` no almacena el nombre sino el código de autor donde los datos de dicho autor (como el nombre, dirección, etc) se almacenan en otra tabla (relacionada por dicho campo) llamada ``tbl_autores`` y que se requiere desplegar el campo del nombre de los autores de los datos encontrados.

.. code-block:: php

  <?php

  $columns = ['tbl_tesis_ejemplo.id', 'tbl_tesis_ejemplo.titulo', 'tbl_autores.nombre'];

  $conditions = ['author' => '6151'];
  $join = ['[><]tbl_autores' => ['autor' => 'id']]

  $fetchedData = $db->select('tbl_tesis_ejemplo', $columns, $conditions);
  // Equivalente a:
  // SELECT tbl_tesis_ejemplo.id, tbl_tesis_ejemplo.titulo, tbl_autores.nombre
  // FROM tbl_tesis_ejemplo INNER JOIN tbl_autores ON tbl_autores.id = tbl_tesis_ejemplo.autor
  // WHERE tbl_tesis_ejemplo.autor = 6151;

  var_dump($fetchedData);
