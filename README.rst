Tuza
====

Tuza es una biblioteca PHP súper ligera [#r1]_ para el consumo de Base de Datos sobre PDO. Provee una API que abstrae (entre otros) las operaciones ``CRUD`` de las bases de datos como métodos de una *instancia de conexión*.

Tuza es compatible con la mayoría de gestores de bases de datos soportados por PDO, cuatro de código abierto: ***SQLite***, ***MySQL***, ***MariaDB***, ***PostreSQL*** y dos privativos: ***SQL Server*** y ***Oracle***.

Tuza es una bifurcación de Medoo, pero no es compatible con este. La API de Tuza es ligeramente diferente pero algo más estática (para proveer de mayor consistencia) que la de Medoo.

Con Tuza es tremendamente sencillo consultar y persistir datos:

.. code-block:: php

  <?php
  use xibalba\tuza\DbConnection;

  // Crear una instancia de conexión
  $dbConnection = new DbConnection([
    'database_type' => 'sqlite',
    'database_file' => 'sqlitedb.db'
  ]);

  // Obtener datos mediante SELECT
  $data = $dbConnection->select('table', ['id', 'label'], ['LIMIT' => 9]);

  // Crear nuevos datos...
  $uniqid = uniqid();
  $newData = [
    'id' => $uniqid,
    'label' => 'Etiqueta'
  ];

  // ... y persistirlos
  $dbConnection->insert('table', $newData);

  // Actualizar datos
  $dbConnection->update('table',
    ['label' => 'Etiqueta actualizada'],
    ['id' => $uniqid]
  );

  // Eliminar datos
  $dbConnection->delete('table', ['id' => $uniqid]);

Aprendé más de Tuza leyendo su documentación.

@todo agregar enlaces.

@todo agregar licencias.

.. rubric:: Footnotes

.. [#r1] Tuza se compone por sólo cinco archivos: Dos clases, dos interfaces, y un *trait*.
